<?php

class PoliticianDb {

	static function insert($conn, $politician) {

		$filename = rand(0,100000);
		$auxPicture	 = $politician->getPicture();
		$filename = $auxPicture["name"];
		
		if (!move_uploaded_file($auxPicture["tmp_name"], "../assets/images/" . $filename)) {
			//echo "Não foi feito upload de arquivo";
			$filename = "";
			return false;
		}
		else {

			$date = $politician->getArrestDate();
            $date = date('Y-m-d', strtotime(str_replace('/', '-', $date)));

			$sql = "insert into politicians (name, description, picture, arrest_date, created, updated) 
				values ('" . $politician->getName() . "'," .
					" '" . $politician->getDescription() . "'," .
					" '" . $filename . "'," .
					" '" . $date . "'," .
					" CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);";
			$result = $conn->query($sql);

			return $result;
		}

	}

	static function update($conn, $politician) {

		$filename = rand(0,100000);
		$auxPicture	 = $politician->getPicture();
		$filename = $auxPicture["name"];
		
		if ($filename) {
			move_uploaded_file($auxPicture["tmp_name"], "../assets/images/" . $filename);
		}

		$date = $politician->getArrestDate();
        $date = date('Y-m-d', strtotime(str_replace('/', '-', $date)));

		$sql = "update politicians set " .
			    " name = '" . $politician->getName() . "'," .
				" description = '" . $politician->getDescription() . "',";
				
		if ($filename) {
			$sql = $sql . " picture = '" . $filename . "',";
		}

		$sql = $sql . " arrest_date = '" . $date . "'," .
					  " updated = CURRENT_TIMESTAMP " .
					  " where id = " . $politician->getId();
		
		$result = $conn->query($sql);
		
		return $result;

	}

	static function delete($conn, $id) {

		$sql = "delete from politicians where id = " . $id;
		$result = $conn->query($sql);

		if ($result) {
			return true;
		} else {
			return false;
		}

	}

	static function getPolitician($conn, $id) {

		$sql = "select * from politicians
			     where id = '" . $id . "'";
		$query = $conn->query($sql);
		$result = mysqli_fetch_array($query, MYSQL_ASSOC);
		$politician = new Politician ($result["id"],$result["name"],$result["description"],$result["picture"],$result["arrest_date"]);
		return $politician;

	}
	
	static function listPoliticians($conn) {

		$sql = "select id, name, description, picture, arrest_date
				  from politicians order by arrest_date desc"; 
		$table = $conn->query($sql);
		$i = 0;
		$result = null;
		while ($data = mysqli_fetch_array($table, MYSQL_ASSOC)) {
			$politician = new Politician ($data["id"],$data["name"],$data["description"],$data["picture"],$data["arrest_date"]);
			$result[$i] = $politician;
			$i++; 
		}
		return $result; 

	}

}

?>