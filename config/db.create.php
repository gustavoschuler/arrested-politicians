<?php

	require_once 'db.connection.php';

	function createDatabase() {

		$conn = createConnection(null);

		// Create database
		$sql = "CREATE DATABASE arrested CHARACTER SET utf8 COLLATE utf8_general_ci;";
		if ($conn->query($sql) === TRUE) {
		    return " Database created successfully <br> ";
		} else {
		    return " Error creating database: " . $conn->error . " <br> ";
		}

		$conn->close();

	}

	function createTables() {

		$conn = createConnection('arrested');
		$msg = "";

		$sql = "CREATE TABLE users (
					id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
					email VARCHAR(50) NOT NULL,
					password VARCHAR(100) NOT NULL,
					created TIMESTAMP,
					updated TIMESTAMP
				)";

		if ($conn->query($sql) === TRUE) {
		    $msg = "Table users created successfully <br> ";
		} else {
		    $msg = "Error creating table: " . $conn->error . " <br> ";
		}

		$sql = "CREATE TABLE politicians (
					id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
					name VARCHAR(50) NOT NULL,
					picture VARCHAR(150),
					description VARCHAR(500),
					arrest_date TIMESTAMP,
					created TIMESTAMP,
					updated TIMESTAMP
				)";

		if ($conn->query($sql) === TRUE) {
		    $msg = $msg . "Table politicians created successfully <br> ";
		} else {
		    $msg = $msg . "Error creating table: " . $conn->error . " <br> ";
		}

		$conn->close();

		return $msg;

	}

	function insertData() {

		$conn = createConnection('arrested');
		$msg = "";

		$sql = "INSERT INTO users (email, password, created, updated) VALUES (
					'john@doe.com', '" . md5("john") . "', CURDATE(), CURDATE() 
				)";

		if ($conn->query($sql) === TRUE) {
		    $msg = "User created successfully <br> ";
		} else {
		    $msg = "Error creating user: " . $conn->error . " <br> ";
		}

		$sql = "INSERT INTO politicians (name, picture, description, arrest_date, created, updated) VALUES ('Pedro Corrêa', 'pedro_correa.jpg', 'Condenado a sete anos e dois meses de prisão, no processo do Mensalão, o ex-deputado do Partido Progressista é suspeito de ter recebido R$ 5,3 milhões em propinas no esquema de corrupção envolvendo a Petrobras. Correa cumpria pena no regime semiaberto, em Pernambuco, quando teve prisão preventiva decretada e foi transferido para o Paraná, em 13 de abril de 2015.', '2013-12-05', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";

		if ($conn->query($sql) === TRUE) {
		    $msg = $msg . "Politician created successfully <br> ";
		} else {
		    $msg = $msg . "Error creating politician: " . $conn->error . " <br> ";
		}

		$conn->close();

		return $msg;

	}

	$printResult = "";

	$printResult = createDatabase();
	$printResult = $printResult . createTables();
	$printResult = $printResult . insertData();

	echo '<pre>' . $printResult;

?>