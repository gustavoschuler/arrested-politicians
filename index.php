<?php

  session_start();
  $_SESSION['SID'] = session_id();

?>

<!DOCTYPE html>

<html>

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <title>Arrested Politicians</title>
  <link rel=icon href=assets/images/logo.png sizes="16x16" type="image/png">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/reset.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/site.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/container.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/grid.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/header.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/image.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/menu.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/divider.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/dropdown.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/segment.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/button.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/list.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/icon.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/sidebar.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/transition.css">

  <link rel="stylesheet" type="text/css" href="assets/css/custom.css">  

</head>
<body class="pushable">

<!-- Following Menu -->
<div class="ui large top fixed menu transition hidden">
  <div class="ui container">
    <a class="active item">Home</a>
    <a class="item" href="/list.php">Arrested Politicians</a>
    <a class="item">About Us</a>
    <a class="item">Contact</a>
    <div class="right menu">
      <?php
        if (isset($_SESSION['logged'])){
          if ($_SESSION['logged'] == 0) {
            echo '<div class="item"><a class="ui primary button" href="login.php">Log in</a></div>';
          } else {
            echo '<a class="item">' . $_SESSION['user'] . '</a><div class="item"><a class="ui primary button" href="admin.php">Admin</a></div><div class="item"><a class="ui primary button" href="controllers/logout.php">Logout</a></div>';
          }
        } else {
          echo '<div class="item"><a class="ui primary button" href="login.php">Log in</a></div>';
        }
      ?>
    </div>
  </div>
</div>

<!-- Sidebar Menu -->
<div class="ui vertical inverted sidebar menu left">
  <a class="active item">Home</a>
  <a class="item" href="/list.php">Arrested Politicians</a>
  <a class="item">About Us</a>
  <a class="item">Contact</a>
  <?php
    if (isset($_SESSION['logged'])){
      if ($_SESSION['logged'] == 0) {
        echo '<a class="item" href="login.php">Login</a>';
      } else {
        echo '<a class="item" href="admin.php">Admin</a><a class="item" href="controllers/logout.php">Logout</a>';
      }
    } else {
      echo '<a class="item" href="login.php">Login</a>';
    }
  ?>
</div>


<!-- Page Contents -->
<div class="pusher">
  <div class="ui inverted vertical masthead center aligned segment">

    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="toc item">
          <i class="sidebar icon"></i>
        </a>
        <a class="active item">Home</a>
        <a class="item" href="/list.php">Arrested Politicians</a>
        <a class="item">About Us</a>
        <a class="item">Contact</a>
        <div class="right item">
          <?php
            if (isset($_SESSION['logged'])){
              if ($_SESSION['logged'] == 0) {
                echo '<a class="ui inverted button" href="login.php">Login</a>';
              } else {
                echo '<a class="ui inverted"><span>' . $_SESSION['user'] . '</span></a><a class="ui inverted button" href="admin.php">Admin</a><a class="ui inverted button" href="controllers/logout.php">Logout</a>';
              }
            } else {
              echo '<a class="ui inverted button" href="login.php">Login</a>';
            }
          ?>
        </div>
      </div>
    </div>

    <div class="ui text container">
      <h1 class="ui inverted header">
        Oh, my! They're arrested!
      </h1>
      <h2>Look at the pretty face of the cause of your problems.</h2>
      <a class="ui huge primary button" href="/list.php">Take a look <i class="right arrow icon"></i></a>
    </div>

  </div>

  <div class="ui vertical stripe segment">
    <div class="ui middle aligned stackable grid container">
      <div class="row">
        <div class="eight wide column">
          <h3 class="ui header">Take a look at the recently arrested politicians in your country</h3>
          <p>Here you can see the face of those son of a bitches and say hi. That one here >> isn't busted yet, but is just a matter of time.</p>
          <h3 class="ui header">They are always innocent</h3>
          <p>They cry. They are all innocent. Poor little wronged pieces of shit.</p>
        </div>
        <div class="six wide right floated column">
          <img src="assets/images/lula-crying.jpg" class="ui large bordered rounded image">
        </div>
      </div>
      <div class="row">
        <div class="center aligned column">
          <a class="ui huge button" href="/list.php">Check Them Out</a>
        </div>
      </div>
    </div>
  </div>


  <div class="ui vertical stripe quote segment">
    <div class="ui equal width stackable internally celled grid">
      <div class="center aligned row">
        <div class="column">
          <h3>"What a f*$#@*&! website"</h3>
          <p>That is what they all say about us</p>
        </div>
        <div class="column">
          <h3>"There's no living soul that's more honest than mine."</h3>
          <p>
            <img src="assets/images/avatar/squid.png" class="ui avatar image"> <b>Squid</b> Owner at Atibaia Farm
          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="ui vertical stripe segment">
    <div class="ui text container">
      <h3 class="ui header">Breaking The Grid, Grabs Your Attention</h3>
      <p>Instead of focusing on content creation and hard work, we have learned how to master the art of doing nothing by providing massive amounts of whitespace and generic content that can seem massive, monolithic and worth your attention.</p>
      <a class="ui large button">Read More</a>
      <h4 class="ui horizontal header divider">
        <a href="http://semantic-ui.com/examples/homepage.html#">Case Studies</a>
      </h4>
      <h3 class="ui header">Did We Tell You About The News?</h3>
      <p>The Petrobras scandal has long been a thorn in Brazil's side. Over the past two years, over 100 people have been arrested for their alleged involvement, including senators and top executives at Petrobras.</p>
      <a class="ui large button">What's next?</a>
    </div>
  </div>


  <div class="ui inverted vertical footer segment">
    <div class="ui container">
      <div class="ui stackable inverted divided equal height stackable grid">
        <div class="three wide column">
          <h4 class="ui inverted header">About</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Sitemap</a>
            <a href="#" class="item">Contact Us</a>
          </div>
        </div>
        <div class="three wide column">
          <h4 class="ui inverted header">BlaBlaBla</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Privacy Policy</a>
            <a href="#" class="item">FAQ</a>
          </div>
        </div>
        <div class="seven wide column">
          <h4 class="ui inverted header">Arrested Politicians</h4>
          <p>Oh, my! They're arrested!<br/>Look at the pretty face of the cause of your problems.</p>
        </div>
      </div>
    </div>
  </div>
</div>


<script src="assets/library/jquery.min.js"></script>
<script src="assets/dist/components/visibility.js"></script>
<script src="assets/dist/components/sidebar.js"></script>
<script src="assets/dist/components/transition.js"></script>
<script>

  $(document)
    .ready(function() {

      // fix menu when passed
      $('.masthead')
        .visibility({
          once: false,
          onBottomPassed: function() {
            $('.fixed.menu').transition('fade in');
          },
          onBottomPassedReverse: function() {
            $('.fixed.menu').transition('fade out');
          }
        })
      ;

      // create sidebar and attach to menu open
      $('.ui.sidebar').sidebar('attach events', '.toc.item');

    });

</script>

</body>
</html>