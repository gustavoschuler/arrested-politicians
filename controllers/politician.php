<?php

	if (isset($_POST['action'])) {

		include "../config/db.connection.php";
		include "../classes/politician.class.php";
		include "../models/politician.model.php";

		$action = $_POST['action'];

	    switch ($action) {
	        case 'Add':
	            insert();
	            break;
	        case 'Edit':
	        	edit($_POST['id']);
	        	break;
	        case 'Remove':
	            remove($_POST['id']);
	            break;
	        case 'Insert' || 'Update':
	        	$picture = $_FILES['picture'];
	        	$id = null;

	        	if ($action == 'Update') {
	        		$id = $_POST['id'];
	        	}

	        	$politician = New Politician($id,$_POST['name'],$_POST['description'],$picture,$_POST['arrest_date']);
	        	save($politician, $action);
	        	break;
	    }
	} else {

		include "config/db.connection.php";
		include "classes/politician.class.php";
		include "models/politician.model.php";

	}

	function getPolitician($id) {

		$conn = createConnection('arrested');

    	$result = PoliticianDB::getPolitician($conn, $id);

    	$conn->close();

		return $result;

	}

	function getPoliticiansList() {

		$conn = createConnection('arrested');

    	$result = PoliticianDB::listPoliticians($conn);

    	$conn->close();

		return $result;
	}

	function edit($id) {

		$res = [
				'redirect' => true, 
				'location' => '/edit.php?id=' . $id, 
				'message' => ''
			   ];

		echo JSON_ENCODE($res);

	}

	function insert() {

		$res = [
				'redirect' => true, 
				'location' => '/new.php', 
				'message' => ''
			   ];

		echo JSON_ENCODE($res);

	}

	function save($politician, $action) {

		$conn = createConnection('arrested');

		if ($action == "Insert") {
    		$result = PoliticianDB::insert($conn, $politician);
    	} else {
    		$result = PoliticianDB::update($conn, $politician);
    	}

    	$conn->close();

		if ($result) {
			$res = ['redirect' => true, 
					'location' => '/admin.php', 
					 'message' => 'Registro salvo com sucesso!', 
					    'type' => 'success'];
		} else {
			$res = ['redirect' => false, 
			        'location' => '', 
			         'message' => 'ERRO: O registro não foi salvo!', 
			            'type' => 'error'];
		}

		echo JSON_ENCODE($res);

	}
	
	function remove($id) {

		$conn = createConnection('arrested');

		$result = PoliticianDB::delete($conn, $id);		

		$conn->close();

		if ($result) {
			$res = ['redirect' => false, 
					'location' => '', 
					 'message' => 'Registro removido com sucesso!', 
					    'type' => 'success'];
		} else {
			$res = ['redirect' => false, 
			        'location' => '', 
			         'message' => 'ERRO: O registro não foi removido!', 
			            'type' => 'error'];
		}

		echo JSON_ENCODE($res);

	}