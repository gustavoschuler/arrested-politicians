<?php

	session_start();
	header('Content-Type: text/html; charset=UTF-8'); 
	
	include "../config/db.connection.php";
	include "../classes/user.class.php";
	include "../models/user.model.php";

	if( (isset($_POST["email"]) && $_POST["email"] != "") && (isset($_POST["password"]) && $_POST["password"] != "") ){
		
		$email = addslashes($_POST['email']);
		$password = addslashes($_POST['password']);

		$conn = createConnection('arrested');

		$user = new User($email,$password);
		
		if (!UserDb::auth($conn, $user)){
			header("Location:/login.php");
			$_SESSION["logged"] = "0";
		}else{
			header("Location:../");
			$_SESSION['logged'] = "1";
			$_SESSION['user'] = $user->getEmail();
		}

		$conn->close();

	}

?>