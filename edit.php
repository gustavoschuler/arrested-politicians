<?php

	session_start();
  $_SESSION['SID'] = session_id();
	$user = $_SESSION['user'];

	if (isset($_SESSION["logged"])){
		if ($_SESSION["logged"] == 0){
		  header("Location:/login.php");
		}
	}else{
		header("Location:/login.php");
	}

	require "controllers/politician.php";

?>

<!DOCTYPE html>

<html>

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <title>Arrested Politicians</title>
  <link rel=icon href=assets/images/logo.png sizes="16x16" type="image/png">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/reset.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/site.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/container.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/grid.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/header.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/image.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/menu.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/divider.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/dropdown.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/segment.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/button.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/list.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/icon.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/sidebar.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/transition.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/form.css">
  <link rel="stylesheet" type="text/css" href="assets/library/jquery-ui-1.12.1/jquery-ui.min.css">
  <link rel="stylesheet" type="text/css" href="assets/library/sweetalert/sweetalert2.min.css">

  <link rel="stylesheet" type="text/css" href="assets/css/custom.css">  

</head>
<body class="pushable">

<!-- Following Menu -->
<div class="ui large top fixed menu transition hidden">
  <div class="ui container">
    <a class="item" href="/">Home</a>
    <a class="item" href="/list.php">Arrested Politicians</a>
    <a class="item">About Us</a>
    <a class="item">Contact</a>
    <div class="right menu">
      <?php
        if (isset($_SESSION['logged'])){
          if ($_SESSION['logged'] == 0) {
            echo '<div class="item"><a class="ui primary button" href="login.php">Log in</a></div>';
          } else {
            echo '<a class="item">' . $user . '</a><div class="item"><a class="ui primary button" href="admin.php">Admin</a></div><div class="item"><a class="ui primary button" href="controllers/logout.php">Logout</a></div>';
          }
        } else {
          echo '<div class="item"><a class="ui primary button" href="login.php">Log in</a></div>';
        }
      ?>
    </div>
  </div>
</div>

<!-- Sidebar Menu -->
<div class="ui vertical inverted sidebar menu left">
  <a class="item" href="/">Home</a>
  <a class="item" href="/list.php">Arrested Politicians</a>
  <a class="item">About Us</a>
  <a class="item">Contact</a>
  <?php
    if (isset($_SESSION['logged'])){
      if ($_SESSION['logged'] == 0) {
        echo '<a class="item" href="login.php">Login</a>';
      } else {
        echo '<a class="item" href="admin.php">Admin</a><a class="item" href="controllers/logout.php">Logout</a>';
      }
    } else {
      echo '<a class="item" href="login.php">Login</a>';
    }
  ?>
</div>


<!-- Page Contents -->
<div class="pusher">
  <div class="ui inverted vertical masthead aligned segment">

    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="toc item">
          <i class="sidebar icon"></i>
        </a>
        <a class="item" href="/">Home</a>
        <a class="item" href="/list.php">Arrested Politicians</a>
        <a class="item">About Us</a>
        <a class="item">Contact</a>
        <div class="right item">
          <?php
            if (isset($_SESSION['logged'])){
              if ($_SESSION['logged'] == 0) {
                echo '<a class="ui inverted button" href="login.php">Login</a>';
              } else {
                echo '<a class="ui inverted"><span>' . $_SESSION['user'] . '</span></a><a class="ui inverted button" href="admin.php">Admin</a><a class="ui inverted button" href="controllers/logout.php">Logout</a>';
              }
            } else {
              echo '<a class="ui inverted button" href="login.php">Login</a>';
            }
          ?>
        </div>
      </div>
    </div>

    <div class="ui text container">
    	<h1 class="ui inverted header">
        	
      	</h1>

      <?php

	        $politician = getPolitician($_GET["id"]);

            $date = $politician->getArrestDate();
            $date = date_create($date);
            $date = date_format($date,"d/m/Y");

            echo '<form class="ui inverted form" name="frm" id="frm" action="" method="post" enctype="multipart/form-data">
                    <div class="field">
	                    <div class="ui two column centered grid">
						    <div class="column">
							    <div class="ui inverted segment">
							      	<img class="ui medium image" src="assets/images/' . $politician->getPicture() . '"/>
							    </div>
						 	</div>
						</div>
						<label>Picture</label>
		                <input name="picture" type="file" value="' . $politician->getPicture() . '"/>
                    </div>
                    <div class="two fields">
                    	<div class="required field">
	                    	<label>Name</label>
	                        <input type="text" name="name" id="name" required="required" data-validation="length" data-validation-length="min2" data-validation-error-msg="O campo é obrigatório" title="Politician name" placeholder="Name" value="' . $politician->getName() . '"/>
	                    </div>
	                    <div class="required field">
	                        <label>Date Arrest</label>
	                    	<input type="text" name="arrest_date" id="arrest_date" required="required" data-validation-error-msg="O campo é obrigatório" title="Politician description" placeholder="Arrest date" value="' . $date . '"/>
	                    </div>
                    </div>
                    <div class="required field">
                    	<label>Description</label>
                    	<textarea name="description" id="description" required="required" data-validation="length" data-validation-length="min2" data-validation-error-msg="O campo é obrigatório" title="Politician description" placeholder="Description" value="">' . $politician->getDescription() . '</textarea>
                    </div>
                    <input type="hidden" name="id" value="' . $politician->getId() . '">
                    <button class="ui primary button" type="submit">Save</button>
                  </form>';

	    ?>
    </div>

  </div>


  <div class="ui inverted vertical footer segment">
    <div class="ui container">
      <div class="ui stackable inverted divided equal height stackable grid">
        <div class="three wide column">
          <h4 class="ui inverted header">About</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Sitemap</a>
            <a href="#" class="item">Contact Us</a>
          </div>
        </div>
        <div class="three wide column">
          <h4 class="ui inverted header">BlaBlaBla</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Privacy Policy</a>
            <a href="#" class="item">FAQ</a>
          </div>
        </div>
        <div class="seven wide column">
          <h4 class="ui inverted header">Arrested Politicians</h4>
          <p>Oh, my! They're arrested!<br/>Look at the pretty face of the cause of your problems.</p>
        </div>
      </div>
    </div>
  </div>
</div>


<script src="assets/library/jquery.min.js"></script>
<script src="assets/dist/components/visibility.js"></script>
<script src="assets/dist/components/sidebar.js"></script>
<script src="assets/dist/components/transition.js"></script>
<script src="assets/dist/components/form.js"></script>
<script src="assets/library/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="assets/library/sweetalert/sweetalert2.min.js"></script>
<script>

  $(document)
    .ready(function() {

      // fix menu when passed
      $('.masthead')
        .visibility({
          once: false,
          onBottomPassed: function() {
            $('.fixed.menu').transition('fade in');
          },
          onBottomPassedReverse: function() {
            $('.fixed.menu').transition('fade out');
          }
        })
      ;

      // create sidebar and attach to menu open
      $('.ui.sidebar').sidebar('attach events', '.toc.item');

      $("form[name='frm']").submit(function(e) {

          var formData = new FormData($(this)[0]);
          formData.append('action','Update');

          $.ajax({
              url: "controllers/politician.php",
              type: "POST",
              data: formData,
              success: function (response) {
                
                var res = jQuery.parseJSON(response);

                if (res.message) {
                  swal({   
                        title: "Edit",   
                        text: res.message,   
                        type: res.type,   
                        showCancelButton: false,     
                        confirmButtonText: "Ok"
                      }).then(function() {
                        if(!res.redirect) {

                          setTimeout(function() {
                             location.reload();
                          }, 0001);

                        } else {

                          window.location.assign(res.location);

                        }
                      });
                }

              },
              cache: false,
              contentType: false,
              processData: false
          });

          e.preventDefault();
      });

    });

    $( function() {
      $( "#arrest_date" ).datepicker( {
        dateFormat: "dd/mm/yy"
      });
    });

</script>

</body>
</html>